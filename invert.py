from PIL import Image
import PIL.ImageOps 
import sys

def convert_to_grey(path):
	img = PIL.ImageOps.invert(Image.open(path))
	img.save('inverted.png')

if len(sys.argv) != 2:
	print("Provide path to image")
else:
	convert_to_grey(sys.argv[1])
