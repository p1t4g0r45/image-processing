from PIL import Image
import sys

def convert_to_grey(path):
	img = Image.open(path).convert('LA')
	img.save('greyscale.png')

if len(sys.argv) != 2:
	print("Provide path to image")
else:
	convert_to_grey(sys.argv[1])
